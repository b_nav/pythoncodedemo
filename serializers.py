from abc import ABC

from rest_framework import serializers
from hotelauth.models import *
from menus.models import CatZones, MenuCategory, MenuItems
from servicepoints.models import ServicePoints


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'role', 'name')


class LicenseDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServicePoints
        fields = ['id', 'user', 'zone', 'license', 'location', 'zone_name', 'delete']


class HotelSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'name')


class HotelLocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserLocation
        fields = '__all__'


class HotelDetailSerializer(serializers.ModelSerializer):
    # user = HotelSerializer( read_only=True)

    class Meta:
        model = UserDetail
        fields = '__all__'


class CatZoneSerializer(serializers.ModelSerializer):
    class Meta:
        model = CatZones
        fields = ['category_id']


#  FILTER ITEMS IN SERIALIZER

class FilteredItemSerializer(serializers.ListSerializer, ABC):
    def to_representation(self, data):
        data = data.filter(delsts=0, actsts=1)
        return super(FilteredItemSerializer, self).to_representation(data)


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = MenuItems
        fields = ['id', 'item', 'img', 'info', 'price', 'actsts']
        list_serializer_class = FilteredItemSerializer


#  FILTER CATEGORY IN SERIALIZER

class FilteredCategorySerializer(serializers.ListSerializer, ABC):
    def to_representation(self, data):
        data = data.filter(actsts=1)
        return super(FilteredCategorySerializer, self).to_representation(data)


class MenuCatSerializer(serializers.ModelSerializer):
    items = ItemSerializer(many=True, read_only=True)

    class Meta:
        model = MenuCategory
        fields = ['id', 'category', 'actsts', 'items', 'delsts']
        list_serializer_class = FilteredItemSerializer
