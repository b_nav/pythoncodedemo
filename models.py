from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db import models
from rest_framework.authtoken.models import Token


class CustomerDetail(models.Model):
    objects = None
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.SET_NULL)
    socialid = models.CharField(blank=True, max_length=100)
    provider = models.CharField(max_length=20, default="fb")
    img_url = models.CharField(blank=True, max_length=255)
    created = models.DateTimeField(auto_now_add=True)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    Token.objects.get_or_create(user=instance)
