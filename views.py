from rest_framework import authentication, permissions
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

import customer
import facebook
import hotelauth
import menus
import random
from customer.models import CustomerDetail
from customer.serializers import *
from hotelauth import models
from promotions.models import Promotion
from promotions.serializers import PromotionSerializer
from servicepoints.models import ServicePoints, Zones


class SocialLogin(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        access_token = request.data.get('accessToken')
        try:
            graph = facebook.GraphAPI(access_token=access_token)
            user_info = graph.get_object(
                id='me',
                fields='id, name,email')
        except facebook.GraphAPIError:
            return Response({"status": False, 'error': 'Invalid data'})

        try:
            customerdetail = CustomerDetail.objects.get(socialid=user_info.get('id')).user_id
            user = models.User.objects.get(id=customerdetail)
            serializer = CustomerSerializer(user)
            json = serializer.data
            token, _ = Token.objects.get_or_create(user=user)
            token = Token.objects.get(key=token)
            json['token'] = token.key
            json['fl'] = 1
            return Response({"user": json, 'status': True})

        except:
            password = models.User.objects.make_random_password()

            if user_info.get('email'):
                email = user_info.get('email')
            else:
                email = user_info.get('id')
            user = models.User(
                name=user_info.get('name'),
                email=email,
                username=email,
                role=2,
            )
            user.set_password(password)
            user.save()
            userid = user.id
            userdetail = customer.models.CustomerDetail(
                user_id=userid,
                socialid=user_info.get('id'), )
            userdetail.save()

            customerdetail = CustomerDetail.objects.get(socialid=user_info.get('id')).user_id
            user = models.User.objects.get(id=customerdetail)
            serializer = CustomerSerializer(user)
            json = serializer.data

            token, _ = Token.objects.get_or_create(user=user)
            token = Token.objects.get(key=token)
            json['token'] = token.key
            json['fl'] = 0

            return Response({"user": json, 'status': True})


class GetHotelDetail(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        licno = request.data['lic']
        try:
            getuser = ServicePoints.objects.get(license=licno)
            serializer = LicenseDetailSerializer(getuser)
            servicepoint = serializer.data
            zonestatus = Zones.objects.get(id=servicepoint['zone']).status

            if servicepoint['delete'] == 0 and zonestatus == 1:
                user = hotelauth.models.UserLocation.objects.get(user=servicepoint['user'])
                hotel = HotelLocationSerializer(user)
                hoteldata = hotelauth.models.User.objects.get(id=servicepoint['user'])
                Hotelauth = HotelSerializer(hoteldata)
                hoteljson = Hotelauth.data

                hotelDetail = hotelauth.models.UserDetail.objects.get(user=servicepoint['user'])
                details = HotelDetailSerializer(hotelDetail)
                detail = details.data
                hoteljson['img'] = detail['img']
                # Get All categories And Get Menus in Drf
                CatZones = menus.models.CatZones.objects.filter(zone=servicepoint['zone'])
                serializer = CatZoneSerializer(CatZones, many=True)
                allcategory = []
                for zoneid in serializer.data:
                    menucat = menus.models.MenuCategory.objects.filter(id=zoneid['category_id'], actsts=1, delsts=0)
                    menudata = MenuCatSerializer(menucat, many=True)
                    category = menudata.data
                    if category != []:
                        allcategory.append(category[0])
                return Response({"status": True, "user": hoteljson, "menu": allcategory, "servicepoint": servicepoint})

            else:
                return Response({"status": False, "error": "QR Is Not Active Now ,Please Scan Another QR"})
        except:
            return Response({"status": False, "error": "Scan Valid QR "})


class Guest(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        name = "Guest"
        username = ''.join(random.choice('asdadxbnzjhasdknmzxbmnxjhvjhavai0123456789') for _ in range(30))
        password = models.User.objects.make_random_password()
        user = models.User(
            name=name,
            username=username,
            email=username,
            role=3,
        )
        user.set_password(password)
        user.save()
        user = models.User.objects.get(username=username)
        serializer = CustomerSerializer(user)
        json = serializer.data
        token, _ = Token.objects.get_or_create(user=user)
        token = Token.objects.get(key=token)
        json['token'] = token.key
        return Response({"status": True, 'user': json})


class Promotions(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, id):
        promotions = Promotion.objects.filter(user=id)
        promotionsdata = PromotionSerializer(promotions, many=True)

        return Response({"status": True, "promotions": promotionsdata.data})


class Menu(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, id):
        getuser = ServicePoints.objects.get(id=id)
        serializer = LicenseDetailSerializer(getuser)
        servicepoint = serializer.data
        CatZones = menus.models.CatZones.objects.filter(zone=servicepoint['zone'])
        serializer = CatZoneSerializer(CatZones, many=True)
        allcategory = []
        for zoneid in serializer.data:
            menucat = menus.models.MenuCategory.objects.filter(id=zoneid['category_id'], actsts=1, delsts=0)
            menudata = MenuCatSerializer(menucat, many=True)
            category = menudata.data
            if category != []:
                allcategory.append(category[0])

        return Response({"status": True, "menu": allcategory})
