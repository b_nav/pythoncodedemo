from django.conf.urls import url
from .views import *

urlpatterns = [
    url('social', SocialLogin.as_view(), name='social-lgin'),
    url('hoteldetail', GetHotelDetail.as_view(), name='hotel-detail'),
    url('guest', Guest.as_view(), name='guest'),
    url('promotions/(?P<id>\d*)$', Promotions.as_view(), name='promotions'),
    url('menu/(?P<id>\d*)$', Menu.as_view(), name='menu'),

]
